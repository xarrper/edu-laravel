<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Task;
use App\Http\Repositories\TaskRepository;

class TaskController extends Controller
{

    private $taskRepository;    

    public function __construct(TaskRepository $taskRepository) 
    {
        $this->taskRepository = $taskRepository;
    }

    public function index()
    {
        $tasks = $this->taskRepository->allTasks()
                ->orderBy('created_at', 'asc')->get();
        //обработка данных
        //несколько запросов
        return $tasks;
    }

    public function show($id) // Нужна проверка на существование $id?
    {
        $task = $this->taskRepository->findById($id);
        return $task;
    }
   
    public function create1(Request $request) 
    {
        $data = [
            'name' => 'задача 3'
        ];
        //валидация данных
        return $this->taskRepository->create1($data); //$request->data
    }

    //Общий метод, подойдет под все Controller
    public function create2(Request $request) 
    {
        $data = [
            'name' => 'задача 4'
        ];
        return $this->taskRepository->create2($data); //$request->data
    }

    public function update1(Request $request, $id)
    {
        $data = [
            'name' => 'задача 5'
        ];
        return $this->taskRepository->update1($id, $data); //$request->data
    }

    public function update2(Request $request, $id)
    {
        $data = [
            'name' => 'задача 6'
        ];
        $this->taskRepository->update2($id, $data); //$request->data
    }

    public function destroy($id) 
    {
        $this->taskRepository->delete($id);
    } 
}
