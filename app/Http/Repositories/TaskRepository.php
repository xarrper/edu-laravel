<?php

namespace App\Http\Repositories;

use App\Model\Task;

class TaskRepository extends BaseRepository 
{
    
    public function __construct(Task $model) 
    {
        $this->model = $model;
    }

    //большой запрос с join
    public function allTasks() 
    {
        return $this->model;
    }

    public function findById($id)
    {
        return $this->model->find($id);
    }

    public function create1($data)
    {
        return $this->model->create($data);
    }

    //Общий метод, подойдет под все Controller
    public function create2($data) 
    {
        $model = $this->model();
        return (new $model())->create($data);
    }

    public function update1($id, $data)
    {
        return $this->model->where('id', $id)->update($data);
    }

    public function update2($id, $data)
    {
        $task = $this->model->find($id);    
        $task->fill($data);
        return $task->save();
    }

    public function delete($id)
    {
        return $this->model->find($id)->delete();
    }

}