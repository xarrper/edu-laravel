<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



    Route::get('/','TaskController@index');
    Route::get('/{id}','TaskController@show')->where('id', '[0-9]+');
    Route::get('/create1','TaskController@create1');
    Route::get('/create2','TaskController@create2');
    Route::get('/update1/{id}','TaskController@update1');
    Route::get('/update2/{id}','TaskController@update2');
    Route::get('/destroy/{id}','TaskController@destroy');
